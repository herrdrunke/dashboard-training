var gulp = require('gulp');
var sass = require('gulp-sass');
// var concat = require('gulp-concat');
// var uglify = require('gulp-uglify');
// var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');

var dir = {
  app: '',
  dest: ''
}

gulp.task('styles', function() {
    gulp.src('sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        // .pipe(concat('bundle.css'))
        // .pipe(minifyCss())
        .pipe(rename({
          extname: '.css'
        }))
        .pipe(gulp.dest('./css/'));
});

//Watch task
gulp.task('default', function() {
    gulp.watch('sass/**/*.scss', ['styles']);
});
